
let count = Number(prompt("Number: "));
console.log('The number you provided is ' + count);

for (count; count > 0; count--){
	
	if(count <= 50){
		console.log("The current value is at " + count + ". Terminating the loop.")
		break;
	}
	
	if(count % 10 === 0){
		console.log('The number you provided - ' + count + ' - is divisible by 10. Skipping the number.');
		continue;
	}


	if(count % 10 !== 0 && count % 5 === 0){
		console.log(count);
		continue;
	}


}


let word = "supercalifragilisticexpialidocious";
console.log(word);
let wordConsonantOnly = ""

for (let i = 0; i < word.length; i++){

	if(
		word[i].toLowerCase() === "a" ||
		word[i].toLowerCase() === "e" ||
		word[i].toLowerCase() === "i" ||
		word[i].toLowerCase() === "o" ||
		word[i].toLowerCase() === "u"
		){} else {
			wordConsonantOnly = wordConsonantOnly += word[i];

		}
	}

console.log(wordConsonantOnly);


// Alternative implementation
/*let word = "supercalifragilisticexpialidocious";
console.log(word);
let wordConsonantOnly = ""

for (let i = 0; i < word.length; i++){

	if(
		word[i].toLowerCase() !== "a" &&
		word[i].toLowerCase() !== "e" &&
		word[i].toLowerCase() !== "i" &&
		word[i].toLowerCase() !== "o" &&
		word[i].toLowerCase() !== "u"
		){
		wordConsonantOnly = wordConsonantOnly += word[i];
		} 
	}

console.log(wordConsonantOnly);*/